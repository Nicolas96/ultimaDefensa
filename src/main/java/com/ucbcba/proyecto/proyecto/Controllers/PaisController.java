package com.ucbcba.proyecto.proyecto.Controllers;


import com.ucbcba.proyecto.proyecto.Entities.Direccion;
import com.ucbcba.proyecto.proyecto.Entities.Empresa;
import com.ucbcba.proyecto.proyecto.Entities.Option;
import com.ucbcba.proyecto.proyecto.Entities.Pais;
import com.ucbcba.proyecto.proyecto.Services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class PaisController {

    private UserService userService;
    private CiudadService ciudadService;
    private PaisService paisService;

    @Autowired
    public void setUserService(UserService userService){this.userService=userService;}

    @Autowired
    private void setCiudadService(CiudadService ciudadService){
        this.ciudadService=ciudadService;
    }

    @Autowired
    private void setPaisService(PaisService paisService){this.paisService = paisService;}

    @RequestMapping(value = "/admin/pais", method = RequestMethod.POST)
    public String save(@Valid Pais pais, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String name = auth.getName();
            model.addAttribute("user",userService.findByEmail(name));
            return "paisForm";
        }
        paisService.savePais(pais);
        return "redirect:/admin/paises";
    }

    @RequestMapping(value = "/admin/paises", method = RequestMethod.GET)
    public String list(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("user",userService.findByEmail(name));
        model.addAttribute("paises",paisService.listAllPaises());
        return "paises";
    }
    @RequestMapping(value = "/admin/pais/new",method = RequestMethod.GET)
    public String newPais(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("user",userService.findByEmail(name));
        model.addAttribute("pais",new Pais());
        model.addAttribute("ciudades",ciudadService.listAllCiudades());
        return "paisForm";
    }
    @RequestMapping(value = "/admin/pais/eliminar/{id}",method = RequestMethod.GET)
    public String deletePais(@PathVariable Integer id, Model model){
        paisService.deletePais(id);
        return "redirect:/admin/paises";
    }
    @RequestMapping(value = "/admin/pais/editar/{id}",method = RequestMethod.GET)
    public String editarPais(@PathVariable Integer id, Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("user",userService.findByEmail(name));
        model.addAttribute("pais",paisService.getPaisById(id));
        model.addAttribute("ciudades",ciudadService.listAllCiudades());
        return "paisForm";
    }
    @RequestMapping(value = "/admin/pais/{id}",method = RequestMethod.GET)
    public String showEmpresa(@PathVariable Integer id, Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("user",userService.findByEmail(name));
        model.addAttribute("pais",paisService.getPaisById(id));
        model.addAttribute("ciudades",ciudadService.listAllCiudades());
        return "pais";
    }

}
